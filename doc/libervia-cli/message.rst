.. highlight:: sh

================================
message: chat message management
================================

Message commands let you send chat messages or manage your server message archives.

.. _libervia-cli_message_send:

send
====

Send a message to a contact or a chat room.
``stdin`` is used as message source.
You can encrypt your message using ``--encrypt [ALGORITHM]`` argument, this will create an encrypted session and replace existing one if needed.
You can manage your encrypted session using ``encryption`` command.

It's possible to send a message to several participants at once (besides group chat),
using addressing commands like ``--to``, ``--cc``, or ``--bcc``. This works in a similar
way as for emails. If your server supports it (i.e., if it supports `XEP-0033`_), only
one message will be sent from Libervia, and the server will handle the rest. If it doesn't
support multicasting, Libervia will handle it itself and send as many messages as
necessary.

You can also add metadata to the message to indicate who must get the reply (with
``--reply-to`` and ``reply-room``) or if the message doesn't expect any reply (with
``--no-reply``).

examples
--------

Send a message to a contact::

  $ echo 'Salut à Toi!' | li message send louise@example.net

Send a message encrypted with OMEMO::

  $ echo 'pssst, this message is encrypted' | li message send -e omemo louise@example.net

Send a ``normal`` message marked as French with a subject::

  $ echo 'Bonjour, je vous écris avec « Libervia »' | li message send -l fr -t normal -S 'Ceci est un message de test'

retract
=======

Retract a message, i.e. mark it as retracted in database, and send a retraction request to
original recipient.

When a message is marked as retracted in database, it won't appear anymore or a hint
(commonly called *tombstone*) will be displayed instead (the behaviour depend of the
frontend that you're using). However, there is an option to archive retracted messages:
``Privacy``/``retract_history``. This option is disabled by default, but if you set it
(e.g. with ``li param set``), the original message will be kept in metadata and may be
displayed if the frontend that you're using allows it.

The ``message_id`` positional argument is the internal ID of the message (not an XMPP ID).
It may be displayed by some frontends.

.. note::

   It is not possible to be sure that a message will be retracted: once something is sent
   through the network, any recipient can keep it, copy it, share it, etc. This is true for
   Libervia/XMPP as for any software, decentralized or not.

   Retract send a retractation **request**, i.e. it asks to the recipient(s) client(s) to
   hide or delete the message, but it's not possible to have a guarantee that nobody kept
   a request.

   The message is also removed from database, except if the option
   ``Privacy``/``retract_history`` is set (see above).

example
-------

Retract message with ID ``1234-5678-9abc-def0``::

  $ li message retract 1234-5678-9abc-def0

mam
===

Query archives using MAM.

This command allows you to check message archive kept on the server (i.e. not the local copy).
You usually want to specify a starting point, and a number of message to retrieve. If too many messages
are available, you'll have to use RSM commands to navigate through the results.

examples
--------

Retrieve messages from last 2 days::

  $ li message mam -S "2 days ago"

Retrieve messages from last 5 hours on Libervia official chat room::

  $ li message mam -S "5 hours ago" -s libervia@chat.jabberfr.org

Retrieve 2 first messages of 2019 on Libervia official chat room::

  $ li message mam -S 2019-01-01 -s libervia@chat.jabberfr.org -m 2

.. _XEP-0033: https://xmpp.org/extensions/xep-0033.html
