==========================
gateway: Gateway management
==========================

``gateway`` command allows you to list, register, and unregister from gateways (usually used to access other networks like ActivityPub or Email) on an XMPP server.

list
====

List available gateways on a server. If no server JID is specified, the command will use the server of the current profile.

By default, only available gateways are shown. If you want to see unavailable gateways as well, you can use the ``-v, --verbose`` option.

example
-------

List available gateways on the server of the default profile::

  $ li gateway list

register
========

Register to a gateway using the specified gateway JID. This will usually show a
registration form that you'll have to complete.

example
-------

Register to the IRC gateway on a server::

  $ li gateway register ap.example.org

unregister
==========

Unregister from a gateway using the specified gateway JID. By default, a confirmation is requested before unregistering. You can skip the confirmation by using the ``-f, --force`` option.

example
-------

Unregister from the SMS gateway on a server without confirmation::

  $ li gateway unregister -f irc.example.org
