========================
event: events management
========================

Event commands allows you to create/modify or get data about an event, and to manage your
invitees.

get
===

Get metadata about one or more event(s).

By default the events are displayed in a format readable, and ``--verbose, -v`` can be
used one or more times to show more or less data. As usual, :ref:`libervia-cli_output` can
be used to get the data in a different way.

example
-------

Retrieve events from personal agenda::

  $ li event get

Get details of a next dentist appointment from personal agenda::

  $ li event get -i dentist -vv

.. _libervia-cli_event_create:

create
======

Create a new event. You can specify the starting time of the event with ``-S TIME_PATTERN, --start TIME_PATTERN``, and either its end time with ``-E TIME_PATTERN, --end TIME_PATTERN`` or its duration with ``-D DURATION, --duration DURATION``. ``--start`` and ``--end`` use a full :ref:`time_pattern` when ``--duration`` uses the relative delta part of it. It's mandatory when creating and event to specify starting and ending time (or duration).

If your organsise an item, the ``--rsvp`` flag should be used: it will use the default
RSVP form which ask for attendance. If you want to request more information to your guest,
``--rsvp_json JSON`` can be used: the JSON argument is a data dict as described in
``data_dict_2_data_form`` function where the ``namespace`` key is not necessary (it's set
automatically):

.. autofunction:: libervia.backend.tools.xml_tools.data_dict_2_data_form

If the event links to an other one, ``--external JID NODE ITEM`` can be user

example
-------

Create an event about to celebrate New Year::

  $ li event create -s some_pubsub@example.net -n urn:xmpp:events:0/party -i new_year_eve_2022 --start 2022-12-31 --end 2023-01-01 -d "Party to celebrate new year" -H https://example.net/some_image.jpg "New Year's Eve"

Add the dentist appointement on coming Tuesday to our personal agenda::

  $ li event create -S tuesday -D "1 hour" Dentist


modify
======

This command works in the same way as libervia-cli_event_create_ but modify an existing
event: all arguments are optional, those specified will override the original ones from
the updated event.

If the ending time or duration must be modified, the starting must be specified again.

example
-------

Change the head picture from the New Year event created above::

  $ li event modify -s some_pubsub@example.net -n urn:xmpp:events:0/party -i new_year_eve_2022 -H https://example.net/some_other_image.jpg


invitee
=======

Subcommands to handle guests. Please check :ref:`libervia-cli_event_invitee`.
