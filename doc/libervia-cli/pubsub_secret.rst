.. _libervia-cli_pubsub_secret:

===================================================
pubsub/secret: share, revoke or rotate node secrets
===================================================

``secret`` group commands to handle pubsub e2ee shared secrets.

You can check :ref:`pubsub-encryption` to have overview on how it works.

share
=====

Share node secrets with an entity. This make the encrypted pubsub node fully accessible to
the recipient.

By default, all node secrets are shared with the recipients, this is normally the desired
behaviour as you most of time want the recipient to have full access to the node. However,
it may be possible to share only some keys by using ``-k ID, --key ID`` argument.

example
-------

Louise want to give access to her private blog to pierre::

  $ li pubsub share secret -n private_blog pierre@example.net

revoke
======

Mark a shared secret as revoked, which means that no new items must be create using this
secret. The secret is still available to decrypt older items.

By default, the revocation notification is sent to all people known to have the latest
shared secret, this is the recommended way to use this command. However, you may send the
revocation notification only to some entities by using ``-r JID, --recipient JID`` as many
times as necessary.

It is usually better to use the rotate command below, which automatically revoke all
existing keys and create a new one, sending suitable notifications.

example
-------

Louise wants to revoke a shared secret used in her private blog::

  $ li pubsub secret revoke -n private_blog k4XBRW9rkYQeGN5fiqoK4R

rotate
======

Revoke all shared secrets of a node, and create a new one. Suitable revocation and secret
sharing notifications are sent.

By default, notifications are sent to all people known to have the latest shared secret.
This is generally what is desirable, unless one or more people who previously had access
to should now be excluded from access to the new items. In this case, you may use the ``-r
JID, --recipient JID`` argument as many times as necessary to specify who should receive
the notifications and new secret.

example
-------

Louise wants to rotate shared secrets of her private blog::

  $ li pubsub secret rotate -n private_blog

list
====

List all known shared secrets of a node.

By default, the private key is not returned, to prevent it from being accidentally
displayed on the screen. If you want to see them too, increase the verbosity with the
``--verbose, -v`` argument.

example
-------

Louise wants to see all secrets used in her private blog::

  $ li pubsub secret list -n private_blog
