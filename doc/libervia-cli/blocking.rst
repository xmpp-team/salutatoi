.. _libervia-cli_blocking:

===========================
blocking: entities blocking
===========================

``blocking`` are commands to block or unblock users by their JIDs (as specified by
`XEP-0191`_). You server must implement this XEP to use those commands.

A blocking is usually done using bare JID, however, you may specify a resource if you want
to block only this resource, or a domain if you want to block a whole domain. Please check
`XEP-0191 § JID Matching`_ for details.

list
====

List already blocked users.

examples
--------

Check who has already been blocked::

  $ li blocking list

block
=====

Block one or more entities by specifying their JIDs.

You just need to specify the bare JIDs of users that you want to block as positional
argument, specify only a domain if you want to block a whole domain.

examples
--------

Louise wants to block ``spammer@example.com`` and the whole domain ``spammers.example``::

  $ li blocking block spammer@example.com spammers.example

unblock
=======

``unblock`` works the same way as ``block``.

If you want to unblock all blocked users at once, you can use the ``all`` keyword. In this
case, you'll have to confirm the action. If you don't want to confirm manually, you can
use the ``-f, --force`` flag.

examples
--------

Pierre wants to unblock the domain ``example.com`` that he has blocked earlier::

  $ li blocking unblock example.com

Élysée want to unblock all blocked users, without confirmation::

  $ li blocking unblock -f all


.. _XEP-0191: https://xmpp.org/extensions/xep-0191.html
.. _XEP-0191 § JID Matching: https://xmpp.org/extensions/xep-0191.html#matching
