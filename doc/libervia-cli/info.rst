
==================================
info: retrieve various information
==================================

``info`` groups subcommands used to retrieve read-only informations.

disco
=====

Display discovery information, including external discovery (see `XEP-0030`_  and
`XEP-0215`_ for details). This can be used to check which features you server or a service
is offering, which items are available (items can be services like chat room, gateways,
etc), ans which external services (i.e. non-XMPP services) are proposed.

You only have to specify the jid of the entity to check, and optionally a node. If a node
is specified, external services won't be retrieved even if ``all`` is used (as external
services don't have the notion of "node").

By default everything is requested (infos, items and external services), but you can
restrict what to request by using ``-t {infos,items,both,external,all}, --type
{infos,items,both,external,all}`` where ``both`` means infos and items, and ``all`` mean
than + external services.


.. _XEP-0030: https://xmpp.org/extensions/xep-0030.html
.. _XEP-0215: https://xmpp.org/extensions/xep-0215.html


example
-------

Request infos, items and external services from a server::

  $ li info disco example.org

version
=======

Request software version of an entity. You only need to specify the jid of the entity as
positional argument.

Depending of the software and its configuration, you have software version, software name,
and the operating system on which the software is running.

example
-------

Check version of a server::

  $ li info version example.org

session
-------

Give information about the session of the given profile. You'll get the full jid currently
used on the server, and the time when the session was started (which may not be the same
time as when the connection with the XMPP server was started).

example
-------

Get session informations::

  $ li info session

devices
-------

List known devices for an entity. You'll get resource name, and data such as presence
data, and identities (i.e. name and type of the client used).

If entity's bare jid is not specified, a list of your own devices is returned.

example
-------

List known devices of Louise::

  $ li info devices louise@example.org

Check if we have other devices connected::

  $ li info devices
