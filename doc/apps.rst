.. _libervia-app-config:

=================================
Libervia App Configuration (YAML)
=================================

The Libervia application uses a YAML configuration file to define various aspects of the
application's behavior, environment, and settings.

The file will be parsed by `PyYAML`_ which is a full-featured YAML implementation. Check
its documentation for details.

Below is the documentation explaining the structure and functionality of each field in the
YAML file.

=================
Root-Level Fields
=================

- ``type``: Specifies the type of the application. See :ref:`libervia-app_type`.

- ``prepare``: Information required to prepare the environment. See :ref:`libervia-app_prepare`.

- ``files``: Specifies files to be created with defined content. See :ref:`libervia-app_files`.

- ``override``: Allows to override or add to default configurations. See :ref:`libervia-app_override`.

- ``expose``: Specifies configurations exposed to frontends or administrators. See :ref:`libervia-app_expose`.

==================
Detailed Sections
===================

.. _libervia-app_type:

type
^^^^

Currently, the only supported type is ``docker-compose``.

.. _libervia-app_prepare:

prepare
^^^^^^^

The ``prepare`` section specifies things like git repositories to be cloned before running the application.

Example
-------
Cloning the repository at `https://example.org/some/repository.git` to prepare the app:

.. code-block:: yaml

  prepare:
    git: https://example.org/some/repository.git

.. _libervia-app_files:

files
^^^^^

The ``files`` section specifies additional files to be created for the application.

The YAML key is the name of the file to be created, and the content specified will
populate that file.

Example
-------
Creating a file named `settings-override.py` with the content `USE_X_FORWARDED_HOST = True`:

.. code-block:: yaml

  files:
    settings-override.py:
      content: |
        USE_X_FORWARDED_HOST = True

.. _libervia-app_override:

override
^^^^^^^^

The ``override`` section allows for the specification or override of configurations. This
creates a ``docker-compose.override.yml`` file that will be merged with the default
``docker-compose.yml``. For more information, see `Docker documentation
<https://docs.docker.com/compose/reference/#specifying-multiple-compose-files>`_.

Example
-------
Overriding the `ports` for the `example_app` service to expose port `8080`:

.. code-block:: yaml

  override:
    services:
      example_app:
        ports:
          - "8080"

.. _libervia-app_expose:

Exposing Data
^^^^^^^^^^^^^

The `expose` section specifies the configurations that are exposed to users or frontends.
See :ref:`libervia-app_param` for a reference of some exposed data. Ports and generated
passwords are exposed in dicts, respectively at the ``ports`` and  ``passwords`` key.

.. _libervia-app_yaml-tags:

YAML Tags
^^^^^^^^^

The following YAML tags can be used in the Libervia App configuration file:

.. _libervia-app_conf:

!libervia_conf
++++++++++++++

Get a value from Libervia configuration. A list is expected with either:

- `name` of a config parameter
- `section` and `name` of a config parameter
- `section`, `name`, and `default` value of a config parameter
- `section`, `name`, `default` value, and `filter` (either `first` or `not`)

Filter options:

- `first`: get the first item of the value
- `not`: get the opposite value (to be used with booleans)

Example
-------
Getting a value from Libervia configuration:

.. code-block:: yaml

  !libervia_conf [section, name, default]

.. _libervia-app_generate_pwd:

!libervia_generate_pwd
+++++++++++++++++++++

Generate a password and store it in persistent data. If the password has already been generated previously, it is reused.

Arguments:

- `name` (str) **required**: Name of the password. Will be used to store it so it kept
  between restarts.
- `size` (int): Size of the password to generate. Default to 30

Example
-------
Generating a password named ``some_password`` with a size of 32 characters:

.. code-block:: yaml

  !libervia_generate_pwd {name: some_password, size: 32}

.. _libervia-app_param:

!libervia_param
++++++++++++++

Get a parameter specified when starting the application. The value can be either the name
of the parameter to get, or a list as `[name, default_value]`.



Available parameters:

url_prefix
    The internal URL where the app will be served.
web_label
    Label which will be used in Web frontend.
web_url_path
    Public facing URL path which will be used in the web frontend to access the app.
web_external
    If True, the web frontend will open the app in a separated page instead of embedding
    it.
front_url
    Whole URL to access the app. It is specified when the app has its own domain or
    subdomain.

Example
-------
Getting a parameter:

.. code-block:: yaml

  !libervia_param [url_prefix, /some_app]

.. _PyYAML: https://pyyaml.org/

