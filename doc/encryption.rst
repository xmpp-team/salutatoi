.. _encryption:

===========================
Encryption in Libervia/XMPP
===========================

Libervia being an XMPP client, it handles encryption between client and server, then the
message is encrypted between servers, and finally to deliver to target client(s).

This avoid the communication to be accessible from people having access to the network,
but the communications are available to server administrators, or administrators of
services that you may use (e.g. pubsub service).

To make your communications inaccessible to anybody but your recipient(s), end-to-end
encryption (or e2ee) may be used. This page aims to explain roughly how it is done in
Libervia and XMPP so end-user can understand and exploit it correctly.

.. note::

   this page is work in progress, it will be completed over time to explain the whole
   machanism. Is something is not clear, please contact the development team at the XMPP
   room `libervia@chat.jabberfr.org <xmpp:libervia@chat.jabberfr.org?join>`__ to get
   details and help to improve this documentation.


.. _pubsub-encryption:

Pubsub Encryption
=================

By default, pubsub items are in plain text (i.e. not encrypted, beside the normal
encryption between client and server, and between servers). This is often the desired
behaviour as pubsub is often used for public matters (public blogs, events, or other kind
of data).

However, pubsub may also be used for private matters, to keep safe some of your own data,
or to uses all other kind of features privately (private blog, event organization, etc).

To make private pubsub node, in addition to the access model which restrict entities which
can retrieve its item, it is possible to use end-to-end encryption.

Pubsub is not encrypted the same way as messages, because you generally need to access all
items of a pubsub node, even if you get access to the node once items have already been
published.

E2ee is currently done using `OpenPGP`_ (`XEP-0473: OpenPGP for XMPP Pubsub`_). Each item is encrypted
using a **symmetric** encryption, which mean that the same key (called "shared secret") is
used both to encrypt and decrypt an item, and is shared between all people who must access
or publish to the pubsub node (i.e. blog, event calendar, etc). This is done this way to
make it easy to add a new members, who can then access all archives of the node, but this
also means that if the shared secret is compromised (i.e. somebody who should not has
obtained a copy), all items made with this secret are accessible to the persons in
possession of the secret.

To make an encrypted pubsub node accessible to somebody, you need to share the secret with
them.

You can see that as the key of a house: everybody who has a copy of the key can go inside
the house, and bring something or take pictures. Sharing the secret is like making
a copy of the key and giving it to the person.

If you think that your shared secret is compromised (obtained by somebody who shouldn't),
or if you want to remove access to somebody, you can "rotate" the secret, which means that
every existing key is revoked (flagged as "you should not use it anymore to write
something", but you can still use it to read archives), and a new one is created (to write
new items).

With the house analogy, it's like changing the locks, and giving new keys to trusted
people: people with the older keys can't go inside the house anymore, but if they have
taken pictures before the key has been changed, theirs pictures is still in their
possession.

.. attention::

   If you rotate the shared secret, new items are using the new secret, but all items
   which were existing before the secret rotation stay accessible to people who had access
   to former secrets (the pubsub node can refuse access to them though). If malicious
   people had access to items before, they could have made copy anytime, thus there is
   little point in reencrypting everything.

To handle encrypted pubsub node shared secrets from command line, you may use
:ref:`libervia-cli_pubsub_secret`.

.. _OpenPGP: https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP

.. _XEP-0473: OpenPGP for XMPP Pubsub: https://xmpp.org/extensions/xep-0473.html


Pubsub Targeted Encryption
==========================

It is also possible to encrypt a single pubsub item for a restricted set of users. This is
different from the pubsub encryption explained above, as if you want to encrypt for a
different set of users, you need to re-encrypt all concerned items, so this is more
adapted for use cases when you only want to encrypt a few items in a pubsub node.

On the other hand, you have all the properties of the algorithm used (for now, only OMEMO
2 is supported), which means that you can have `Perfect Forward Secrecy`_ for algorithms
supporting it (it's the case for OMEMO.)

.. note::

   Pubsub Targeted Encryption(PTE) specification is not currently an official XEP (XMPP
   Extension Protocol), it is about to be examinated by "XMPP council". This documentation
   will be updated with the evolution of the situation.

.. _Perfect Forward Secrecy: https://en.wikipedia.org/wiki/Forward_secrecy

Pubsub Signature
================

By default, identity of the publisher of a pubsub item is difficult to authenticate: it
may be specified by the pubsub service (using the `"publisher" attribute`_), but this
attribute is not set by all pubsub services, and it can be spoofed by the service or the
XMPP server.

To strongly authenticate the publisher of a pubsub item, it is possible to cryptographically sign an item. This can work with any pubsub item, encrypted or not, and it can be done after the item has been published. The process use `XEP-0476: Pubsub Signing: OpenPGP Profile`_.

.. attention::

   Signature only certifies that the signers strongly link themselves with this version of
   the item, not that the signers are the original authors of the item. In other words, it
   prevents somebody to say that somebody else has published something (the signature
   would be missing or invalid), but the published data may come from anywhere. Also keep
   in mind that a security breach (stolen encryption keys, major bug somewhere) is always
   possible.


To handle pubsub signatures from command line, you may use
:ref:`libervia-cli_pubsub_signature`.

.. _"publisher" attribute: https://xmpp.org/extensions/xep-0060.html#publisher-publish-success-publisher

.. _XEP-0476: Pubsub Signing: OpenPGP Profile: https://xmpp.org/extensions/xep-0476.html

E2EE with Email Gateway
=======================

For compatible gateways, Libervia supports "Relayed" encryption, which is a method for using end-to-end encryption (E2EE). This feature is currently experimental and only works for emails.

If no key is known for an email recipient, Libervia uses Autocrypt. Autocrypt automatically exchanges encryption keys to enable what's called "opportunistic" encryption. While the security isn't as robust as proper key management and peer authentication, it still offers better protection than having no E2EE at all. Autocrypt is only used when a recipient’s key is not known by other, more secure means.

For more information about Autocrypt and its security model, please refer to `Wikipedia's Autocrypt page`_.

.. _Wikipedia's Autocrypt page: https://en.wikipedia.org/wiki/Autocrypt
