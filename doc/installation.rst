.. _installation:

============
Installation
============

These are the instructions to install Libervia using Python.
Note that if you are using GNU/Linux, Libervia may already be present on your distribution.

Libervia is made of one backend, and several frontends. To use it, the first thing to do is to install the backend.

We recommand to use development version for now, until the release of 0.9.

Development version
-------------------

*Note for Arch users: a pkgbuild is available for your distribution on
AUR, check libervia-backend-hg (as well as other libervia-\* packages).*

You can install the latest development version using pip. You need to
have the following dependencies installed first:

-  Python 3 with development headers
-  Python 3 "venv", which may already be installed with Python 3
-  Mercurial
-  libcairo 2 with development header
-  libjpeg with development headers
-  libgirepository 1.0 with development headers
-  libdbus-1 with development headers
-  libdbus-glib-1 with development headers
-  libxml2 with development headers
-  libxlt2 with development headers
-  D-Bus x11 tools (this doesn't needs X11, it is just needed for dbus-launch)
-  cmake
-  Python GPG package (and its GPG dependencies), those are needed to use all OpenPGP
   related features. We need to use the system package as package version needs to match
   system GPG version.

On Debian and derivatives, you can get all this with following command::

  $ sudo apt-get install python3-dev python3-venv python3-wheel mercurial libxml2-dev libxslt-dev libcairo2-dev libjpeg-dev libgirepository1.0-dev libdbus-1-dev libdbus-glib-1-dev dbus-x11 cmake python3-gpg

Installation With pipx
~~~~~~~~~~~~~~~~~~~~~~

If you have `pipx`_ installed, you can easily install the development version of the
backend by entering the following command:

.. code-block:: bash

   $ pipx install --system-site-packages hg+https://repos.goffi.org/libervia-backend#egg=libervia-backend[all]

Installation With Virtual Environment And pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Alternatively, you can install a virtual environment, clone the repository, and pip
install from there.

Go in a location where you can install Libervia, for instance your home directory::

  $ cd

And enter the following commands (here we install Libervia with all extra dependencies
support, i.e. will dependencies needed for optional features)::

  $ python3 -m venv --system-site-packages libervia
  $ source libervia/bin/activate
  $ pip install -U pip wheel
  $ hg clone https://repos.goffi.org/libervia-backend
  $ cd libervia-backend
  $ pip install -e .

Don't worry if you see the following message, Libervia should work anyway::

  Failed building wheel for <some_package_name>

Post Installation
~~~~~~~~~~~~~~~~~

After installing Libervia, you need to install the media (you may skip this if you don't
plan to use any graphical frontend)::

  $ cd
  $ hg clone https://repos.goffi.org/libervia-media

then, create the directory ``~/.config/libervia``::

  $ mkdir -p ~/.config/libervia

and the file ``~/.config/libervia/libervia.conf`` containing:

.. sourcecode:: cfg

  [DEFAULT]
  media_dir = ~/libervia-media

Of course, replace ``~/libervia-media`` with the actual path you have used.

You can check :ref:`configuration` for details

Usage
=====

To launch the Libervia backend, enter::

  $ libervia-backend

…or, if you want to launch it in foreground::

  $ libervia-backend fg

You can stop it with::

  $ libervia-backend stop

To know if backend is launched or not::

  $ libervia-backend status

**NOTE**: if ``misc/org.libervia.Libervia.service`` is installed correctly (which should
be done during the installation), the backend is automatically launched when a frontend
needs it.

You can check that Libervia is installed correctly by trying jp (the backend need to be
launched first, check below)::

  $ li --version
  Libervia CLI 0.8.0D « La Cecília » (rev 184c66256bbc [M] (default 2021-06-09 11:35 +0200) +524) Copyright (C) 2009-2021 Jérôme Poisson, Adrien Cossa
  This program comes with ABSOLUTELY NO WARRANTY;
  This is free software, and you are welcome to redistribute it under certain conditions.

If you have a similar output, Libervia is working.

.. note::

  You may see an error message indicating that D-Bus is not launched or that its
  environment variable is not set, this usually happens when launching Libervia on a
  server, without graphic interface like X.org or Wayland (otherwise D-Bus service should
  be launched automatically). In this case please follow instructions below.

Launching D-Bus (on servers)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  You can launch the D-Bus service by creating a shell script with the following content
  in a file named ``dbus_launch.sh``:

  .. sourcecode:: sh

    #!/bin/sh

    DBUS_PATH="/tmp/.dbus.`whoami`"

    if [ ! -e $DBUS_PATH ]; then
            dbus-launch --sh-syntax > $DBUS_PATH
            chmod 400 $DBUS_PATH
    fi

    cat $DBUS_PATH

  Then run it before the backend or frontend by entering::

    $ eval $(/path/to/dbus_launch.sh)

  This will launch the D-Bus daemon if necessary, and set the appropriate environment
  variable. If you use a new shell, be sure to launch the script again in an ``eval``
  statement to have the environment variable set. You can put this in your ``.zshrc`` (or
  whatever you're using) to make it automatic.

  If you don't want to use D-Bus, you can use another bridge, e.g. ``pb`` is a good
  choice, by updating your :ref:`configuration` ``[DEFAULT]`` section with ``bridge =
  pb``.

Frontends
=========

So far, the following frontends exist and are actively maintained:

Libervia Desktop (aka Cagou)
  desktop/mobile (Android) frontend

Libervia Web
  the web frontend

Libervia TUI (aka Primitivus)
  Text User Interface

Libervia CLI (aka jp or li)
  Command Line Interface

To launch Libervia TUI, just type::

  $ libervia-tui

then create a profile (XMPP account must already exist).

To use Libervia CLI, follow its help (``li`` is a shortcut for ``libervia-cli``)::

  $ li --help


There are some other frontends:

Bellaciao
  based on Qt, a rich desktop frontend (currently on hold)

Wix
  former desktop frontend based on WxWidgets (deprecated with version 0.6.0)

Sententia
  Emacs frontend developed by a third party (development is currently stalled)

.. _pipx: https://pypa.github.io/pipx/
