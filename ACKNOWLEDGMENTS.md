* NOTE: this is coming from old REAME file, and it's not up-to-date, please check
  `pyproject.toml` to see a list of third party projects used as requirements and Mercurial
  logs of the various repositories to get a complete list of contributors*

## CREDIT

A big big thank to the authors/contributors of...

proxy65:
Libervia (plugin_xep_0065) use nearly all the code from proxy65 (http://code.google.com/p/proxy65/) which was coded by Dave Smith (2002-2004) and maintained by Fabio Forno (2007-2008).
As the original MIT licence allows, the code is reused and sub-licenced until GPL v3 to follow the rest of the code.

progressbar:
Libervia (jp) use ProgressBar (http://pypi.python.org/pypi/progressbar/2.2), a class coded by Nilton Volpato which allow the textual representation of progression.

twisted:
Libervia is heavily based on the twisted framework (http://twistedmatrix.com/trac/), a very great tool which offer a lot of protocols management. There are too many contributors to name them here, so take a look on the website :).

wokkel:
Libervia use a library with high level enhancements on top of twisted, which is called wokkel (http://wokkel.ik.nu). Lot of thanks to Ralph Meijer and all other contributors.

Urwid:
Primitivus is based on Urwid (http://excess.org/urwid/) which saved me a lot of time. It's really a great library to easily make a sophisticated interface.

Pyjamas:
Libervia is built with a Pyjamas (http://pyjs.org), a Google Web Toolkit port for python, including Python to Javascript compiler, and Pyjamas Desktop which allow to execute the same application on the desktop or through a browser. It's really an amazing tool.

Kivy and its linked tools (python-for-android, buildozer, plyer, pyjnius):
Kivy and linked tools (https://kivy.org) are used to build Cagou frontend, and to port it on several platforms. Excellent pieces of software, well thought, 

Kivy garden:
in addition to Kivy itself, extension from the garden are used:
- contextmenu: used to display main and context menus

lxml(http://lxml.de/):
this powerful and efficient XML parsing module is used sometimes to replace Twisted internal tools: its API is handy, and it have some features like evil content cleaning.

pillow(https://python-pillow.github.io/):
This image manipulation module is used for avatars

txJSON-RPC:
Libervia use txJSON-RPC (https://launchpad.net/txjsonrpc), a twisted library to communicate with the browser's javascript throught JSON-RPC

Mutagen:
Mutagen (https://bitbucket.org/lazka/mutagen) is an audio metadata handling library, it's used by the radiocol plugin.

Python OTR (http://python-otr.pentabarf.de), PyCrypto (https://www.dlitz.net/software/pycrypto) and pyOpenSSL(https://github.com/pyca/pyopenssl):
Used for cryptography

otr.js and its dependencies Big Integer Library, CryptoJS, EventEmitter:
Libervia frontend uses otr.js and its dependencies:
    - otr.js was coded by Arlo Breault (2014) and is released under the Mozilla Public License Version 2.0
    - Big Integer Library was coded by Leemon Baird (2000-2013) and is in the public domain
    - CryptoJS was coded by Jeff Mott (2009-2013) and is released under the MIT licence
    - EventEmitter was coded by Oliver Caldwell (2011-2013) and is released under the MIT licence
As the original licences allow, the code is reused and sub-licenced until GPL v3 to follow the rest of the code.

mardown (https://pythonhosted.org/Markdown/) and html2text (https://pypi.python.org/pypi/html2text/2015.6.21):
both are used for syntaxes conversions

Jinja2 (http://jinja.pocoo.org/):
a poweful template engine for Python that we use for designing Libervia's static blog pages

miniupnp (http://miniupnp.free.fr/):
this UPnP-IGD implementation is used to facilitate P2P sessions

netifaces (https://pypi.python.org/pypi/netifaces):
when available, this module is used to detect local IPs

pictures found in the libervia_media repository and used by Libervia and Libervia:
Please read the credits and licence information that are given in the README and COPYING files for each work: http://repos.goffi.org/libervia_media/file

the powerfull ImageMagick (http://www.imagemagick.org/) is used by the script written to split the previously named picture.

PyXDF (http://freedesktop.org/wiki/Software/pyxdg):
Used to follow FreeDesktop XDG standards

A special thank to people working on XMPP standards, libre standards are the way to go !

and the others:
and of course, nothing would be possible without Python (http://www.python.org/), GNU and the Free Software Foundation (http://www.gnu.org, http://www.fsf.org/), the Linux Kernel (http://www.kernel.org/), and the coder of the tools we use like Vim (http://www.vim.org/), Mercurial (http://www.selenic.com/mercurial/wiki/), or all the KDE stuff (http://www.kde.org/ and of course http://amarok.kde.org/), and also XFCE (http://www.xfce.org), etc. Thanks thanks thanks, thanks to everybody in the Free (Libre) chain for making a part of this.

If we forgot any credit (and we probably have), please contact us (mail below) to fix it.

## CONTRIBUTORS

Salut à Toi has received contributions from:

- Adrien Vigneron <adrienvigneron@mailoo.org>: huge work on Libervia's CSS, Libervia Logo (the mascot is his work), and Quiz game graphics.

- Xavier Maillard <xavier@maillard.im>: bugs fixes, libervia_templates installation.

- Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>: bugs fixes, Libervia's notification, Libervia as a twisted application plugin.

- Matthieu Rakotojaona <matthieu.rakotojaona@gmail.com>: English translation of the social contract.

- Thomas Preud'homme <robotux@debian.org>: bugs fixes. He's also one of the co-maintainer of the Debian package.

- Dal <kedals0@gmail.com>: profiles management, argparse refactoring in jp.

- Matteo Cypriani <mcy@lm7.fr>: jp's mainloop update + doc improvements + various fixes. He's also the other co-maintainer of the Debian package.

- Olly Betts <olly@survex.com>: icon fix in Wix [N.B.: Wix has since been removed]

- Geoffrey Pouzet <chteufleur@kingpenguin.tk>: XEP-0070 and XEP-0184 implementations

- Arnaud Joset <info@agayon.be>: setup fixes

Many thanks to them.

A big thank also to all the maintainers of Libervia packages.
