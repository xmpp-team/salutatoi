#!/bin/bash

SETUP_INDICATOR="/home/libervia/.local/share/libervia/setup_complete.flag"
FORUMS_JSON_FILE="/home/libervia/forums.json"

if [ ! -f "$SETUP_INDICATOR" ]; then
    echo "First run detected. Setting up the environment and configuration."

    # we create the file sharing component which will autoconnect when backend is started
    libervia-cli profile create file-sharing -j files.server1.test -p "" --xmpp-password test_e2e -C file-sharing -A
    # we have to be sure that file-sharing is connected before demo account,
    # otherwise disco will be cached without it, and feature such as file
    # sharing won't be available.
    libervia-cli profile connect -cp file-sharing
    # create a demo account
    libervia-cli profile create demo -j demo@server1.test -p "demo"
    libervia-cli profile modify -D -cp demo --pwd demo
    libervia-cli forums set -p demo < $FORUMS_JSON_FILE
    libervia-cli bookmarks add -pdemo -a demo@chat.server1.test

    touch "$SETUP_INDICATOR"
    echo "Initial setup complete."
else
    echo "Not the first run. Skipping setup."
fi
