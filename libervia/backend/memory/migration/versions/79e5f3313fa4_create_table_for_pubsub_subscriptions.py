"""create table for pubsub subscriptions

Revision ID: 79e5f3313fa4
Revises: 129ac51807e4
Create Date: 2022-03-14 17:15:00.689871

"""

from alembic import op
import sqlalchemy as sa
from libervia.backend.memory.sqla_mapping import JID


# revision identifiers, used by Alembic.
revision = "79e5f3313fa4"
down_revision = "129ac51807e4"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "pubsub_subs",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("node_id", sa.Integer(), nullable=False),
        sa.Column("subscriber", JID(), nullable=True),
        sa.Column("state", sa.Enum("SUBSCRIBED", "PENDING", name="state"), nullable=True),
        sa.ForeignKeyConstraint(
            ["node_id"],
            ["pubsub_nodes.id"],
            name=op.f("fk_pubsub_subs_node_id_pubsub_nodes"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_pubsub_subs")),
        sa.UniqueConstraint("node_id", "subscriber", name=op.f("uq_pubsub_subs_node_id")),
    )


def downgrade():
    op.drop_table("pubsub_subs")
