"""add origin_id column to history and adapt constraints

Revision ID: 4b002773cf92
Revises: 79e5f3313fa4
Create Date: 2022-06-13 16:10:39.711634

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4b002773cf92"
down_revision = "79e5f3313fa4"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.add_column(sa.Column("origin_id", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            "uq_origin_id", ["profile_id", "origin_id", "source"]
        )

    with op.batch_alter_table("message", schema=None) as batch_op:
        batch_op.alter_column("history_uid", existing_type=sa.TEXT(), nullable=False)
        batch_op.alter_column("message", existing_type=sa.TEXT(), nullable=False)

    with op.batch_alter_table("subject", schema=None) as batch_op:
        batch_op.alter_column("history_uid", existing_type=sa.TEXT(), nullable=False)
        batch_op.alter_column("subject", existing_type=sa.TEXT(), nullable=False)


def downgrade():
    with op.batch_alter_table("subject", schema=None) as batch_op:
        batch_op.alter_column("subject", existing_type=sa.TEXT(), nullable=True)
        batch_op.alter_column("history_uid", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("message", schema=None) as batch_op:
        batch_op.alter_column("message", existing_type=sa.TEXT(), nullable=True)
        batch_op.alter_column("history_uid", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.drop_constraint("uq_origin_id", type_="unique")
        batch_op.drop_column("origin_id")
