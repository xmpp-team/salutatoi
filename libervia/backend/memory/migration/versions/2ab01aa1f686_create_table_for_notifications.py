"""create table for notifications

Revision ID: 2ab01aa1f686
Revises: 4b002773cf92
Create Date: 2023-10-16 12:11:43.507295

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "2ab01aa1f686"
down_revision = "4b002773cf92"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "notifications",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("timestamp", sa.Float(), nullable=False),
        sa.Column("expire_at", sa.Float(), nullable=True),
        sa.Column("profile_id", sa.Integer(), nullable=True),
        sa.Column(
            "type",
            sa.Enum(
                "chat",
                "blog",
                "calendar",
                "file",
                "call",
                "service",
                "other",
                name="notificationtype",
            ),
            nullable=False,
        ),
        sa.Column("title", sa.Text(), nullable=True),
        sa.Column("body_plain", sa.Text(), nullable=False),
        sa.Column("body_rich", sa.Text(), nullable=True),
        sa.Column("requires_action", sa.Boolean(), nullable=True),
        sa.Column("priority", sa.Integer(), nullable=True),
        sa.Column("extra_data", sa.JSON(), nullable=True),
        sa.Column(
            "status", sa.Enum("new", "read", name="notificationstatus"), nullable=True
        ),
        sa.ForeignKeyConstraint(
            ["profile_id"],
            ["profiles.id"],
            name=op.f("fk_notifications_profile_id_profiles"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_notifications")),
    )
    with op.batch_alter_table("notifications", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_notifications_profile_id"), ["profile_id"], unique=False
        )
        batch_op.create_index(
            "notifications_profile_id_status", ["profile_id", "status"], unique=False
        )


def downgrade():
    with op.batch_alter_table("notifications", schema=None) as batch_op:
        batch_op.drop_index("notifications_profile_id_status")
        batch_op.drop_index(batch_op.f("ix_notifications_profile_id"))

    op.drop_table("notifications")
