"""add "version_id" to History

Revision ID: 610345f77e75
Revises: 2ab01aa1f686
Create Date: 2023-11-20 17:33:53.544032

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "610345f77e75"
down_revision = "2ab01aa1f686"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "version_id", sa.Integer(), server_default=sa.text("1"), nullable=False
            )
        )


def downgrade():
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.drop_column("version_id")
