#!/usr/bin/env python3


# SàT: a jabber client
# Copyright (C) 2009-2021 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""email sending facilities"""


from configparser import ConfigParser
from email.mime.text import MIMEText
from twisted.internet import defer
from twisted.mail import smtp
from libervia.backend.core.constants import Const as C
from libervia.backend.tools import config as tools_config
from libervia.backend.core.log import getLogger

log = getLogger(__name__)


def send_email(
    config: ConfigParser,
    to_emails: list[str] | str,
    subject: str = "",
    body: str = "",
    from_email: str | None = None,
) -> defer.Deferred:
    """Send an email using Libervia configuration

    @param config: the configuration instance
    @param to_emails: list of recipients
        if unicode, it will be split to get emails
    @param subject: subject of the message
    @param body: body of the message
    @param from_email: address of the sender
    @return:Same as smtp.sendmail.
    """
    if isinstance(to_emails, str):
        to_emails = to_emails.split()
    email_host = tools_config.config_get(config, None, "email_server") or "localhost"
    email_from = from_email or tools_config.config_get(config, None, "email_from")

    # we suppose that email domain and XMPP domain are identical
    domain = tools_config.config_get(config, None, "xmpp_domain")
    if domain is None:
        if email_from is not None:
            domain = email_from.split("@", 1)[-1]
        else:
            domain = "example.net"

    if email_from is None:
        email_from = "no_reply@" + domain
    email_sender_domain = tools_config.config_get(
        config, None, "email_sender_domain", domain
    )
    email_port = int(tools_config.config_get(config, None, "email_port", 25))
    email_username = tools_config.config_get(config, None, "email_username")
    email_password = tools_config.config_get(config, None, "email_password")
    email_auth = C.bool(tools_config.config_get(config, None, "email_auth", C.BOOL_FALSE))
    email_starttls = C.bool(
        tools_config.config_get(config, None, "email_starttls", C.BOOL_FALSE)
    )

    msg = MIMEText(body, "plain", "UTF-8")
    msg["Subject"] = subject
    msg["From"] = email_from
    msg["To"] = ", ".join(to_emails)

    return smtp.sendmail(
        email_host.encode("utf-8"),
        email_from.encode("utf-8"),
        [email.encode("utf-8") for email in to_emails],
        msg.as_bytes(),
        senderDomainName=email_sender_domain if email_sender_domain else None,
        port=email_port,
        username=email_username.encode("utf-8") if email_username else None,
        password=email_password.encode("utf-8") if email_password else None,
        requireAuthentication=email_auth,
        requireTransportSecurity=email_starttls,
    )
