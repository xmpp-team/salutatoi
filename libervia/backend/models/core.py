#!/usr/bin/env python3

# Libervia common models
# Copyright (C) 2009-2023 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pydantic import BaseModel, Field


class MessageData(dict):
    pass


class MessageUpdateData(BaseModel):
    type: str

    def __init__(self, **kwargs):
        if "type" not in kwargs:
            kwargs["type"] = "reactions"
        super().__init__(**kwargs)


class MessageReactionData(MessageUpdateData):
    reactions: dict[str, list[str]] = Field(
        description="Reaction to reacting entities mapping"
    )


class MessageEdition(BaseModel):
    """Data used to send a message edition"""

    message: dict[str, str]
    subject: dict[str, str] = Field(default_factory=dict)
    extra: dict[str, str] = Field(default_factory=dict)


class DiscoIdentity(BaseModel):
    """A single disco identity

    Note that an XMPP entity may have several disco identities.
    """

    name: str
    category: str
    type: str
