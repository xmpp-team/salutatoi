#!/usr/bin/env python3

# Copyright (C) 2009-2022 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import Self

from pydantic import BaseModel, Field
from twisted.words.xish import domish

from libervia.backend.core import exceptions
from libervia.backend.core.constants import Const as C
from libervia.backend.core.i18n import _
from libervia.backend.core.log import getLogger

log = getLogger(__name__)

PLUGIN_INFO = {
    C.PI_NAME: "URL Address Information",
    C.PI_IMPORT_NAME: "XEP-0103",
    C.PI_TYPE: "XEP",
    C.PI_MODES: C.PLUG_MODE_BOTH,
    C.PI_PROTOCOLS: ["XEP-0103"],
    C.PI_MAIN: "XEP_0103",
    C.PI_HANDLER: "no",
    C.PI_DESCRIPTION: _("""Implementation of XEP-0103 (URL Address Information)"""),
}
NS_URL_DATA = "http://jabber.org/protocol/url-data"


class Desc(BaseModel):
    """
    Model for the <desc/> element.
    """

    content: str
    xml_lang: str | None = None


class URLData(BaseModel):
    """
    Model for the <url-data/> element.
    """

    url: str
    desc: list[Desc] = Field(default_factory=list)

    @classmethod
    def from_element(cls, element: domish.Element) -> Self:
        """Create a URLData instance from a <url-data> element or its parent.

        @param url_data_elt: The <url-data> element or a parent element.
        @return: URLData instance.
        @raise exceptions.NotFound: If the <url-data> element is not found.
        """
        if element.uri != NS_URL_DATA or element.name != "url-data":
            child_url_data_elt = next(element.elements(NS_URL_DATA, "url-data"), None)
            if child_url_data_elt is None:
                raise exceptions.NotFound("<url-data> element not found")
            else:
                element = child_url_data_elt
        kwargs = {
            "url": element["target"],
            "desc": [
                Desc(content=str(desc_elt), xml_lang=desc_elt.getAttribute("xml:lang"))
                for desc_elt in element.elements(NS_URL_DATA, "desc")
            ],
        }
        return cls(**kwargs)

    def to_element(self) -> domish.Element:
        """Build the <url-data> element from this instance's data.

        @return: <url-data> element.
        """
        url_data_elt = domish.Element((NS_URL_DATA, "url-data"))
        url_data_elt["target"] = self.url
        for desc in self.desc:
            desc_elt = url_data_elt.addElement((NS_URL_DATA, "desc"))
            if desc.xml_lang:
                desc_elt["xml:lang"] = desc.xml_lang
            desc_elt.addContent(desc.content)
        return url_data_elt


class XEP_0103:
    namespace = NS_URL_DATA

    def __init__(self, host):
        log.info(f"plugin {PLUGIN_INFO[C.PI_NAME]!r} initialization")
        host.register_namespace("url-data", NS_URL_DATA)

    def generate_url_data(self, url: str, **kwargs) -> URLData:
        """Generate the element describing the URL

        @param url: URL to use
        @param extra: extra metadata describing how to access the URL
        @return: ``<url-data/>`` element
        """
        url_data = URLData(url=url, **kwargs)
        return url_data

    def parse_url_data_elt(self, url_data_elt: domish.Element) -> URLData:
        """Parse <url-data/> element

        @param url_data_elt: <url-data/> element
            a parent element can also be used
        @return: URLData instance
        @raise exceptions.NotFound: no <url-data/> element has been found
        """
        return URLData.from_element(url_data_elt)
