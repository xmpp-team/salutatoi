#!/usr/bin/env python3

# Libervia plugin for XMPP Date and Time Profile formatting and parsing with Python's
# datetime package
# Copyright (C) 2022-2022 Tim Henkes (me@syndace.dev)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libervia.backend.core.constants import Const as C
from libervia.backend.core.i18n import D_
from libervia.backend.core.main import LiberviaBackend
from libervia.backend.tools import xmpp_datetime


__all__ = ["PLUGIN_INFO", "XEP_0082"]  # pylint: disable=unused-variable


PLUGIN_INFO = {
    C.PI_NAME: "XMPP Date and Time Profiles",
    C.PI_IMPORT_NAME: "XEP-0082",
    C.PI_TYPE: C.PLUG_TYPE_MISC,
    C.PI_PROTOCOLS: ["XEP-0082"],
    C.PI_DEPENDENCIES: [],
    C.PI_RECOMMENDATIONS: [],
    C.PI_MAIN: "XEP_0082",
    C.PI_HANDLER: "no",
    C.PI_DESCRIPTION: D_("Date and Time Profiles for XMPP"),
}


class XEP_0082:  # pylint: disable=invalid-name
    """
    Implementation of the date and time profiles specified in XEP-0082 using Python's
    datetime module. The legacy format described in XEP-0082 section "4. Migration" is not
    supported. Reexports of the functions in :mod:`sat.tools.xmpp_datetime`.

    This is a passive plugin, i.e. it doesn't hook into any triggers to process stanzas
    actively, but offers API for other plugins to use.
    """

    def __init__(self, sat: LiberviaBackend) -> None:
        """
        @param sat: The SAT instance.
        """

    format_date = staticmethod(xmpp_datetime.format_date)
    parse_date = staticmethod(xmpp_datetime.parse_date)
    format_datetime = staticmethod(xmpp_datetime.format_datetime)
    parse_datetime = staticmethod(xmpp_datetime.parse_datetime)
    format_time = staticmethod(xmpp_datetime.format_time)
    parse_time = staticmethod(xmpp_datetime.parse_time)
