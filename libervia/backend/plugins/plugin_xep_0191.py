#!/usr/bin/env python3

# Libervia plugin for XEP-0191
# Copyright (C) 2009-2022 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import List, Set

from twisted.words.protocols.jabber import xmlstream, jid
from twisted.words.xish import domish
from twisted.internet import defer
from zope.interface import implementer
from wokkel import disco, iwokkel

from libervia.backend.core.constants import Const as C
from libervia.backend.core.i18n import _
from libervia.backend.core.log import getLogger
from libervia.backend.core.core_types import SatXMPPEntity
from libervia.backend.tools.utils import ensure_deferred

log = getLogger(__name__)

PLUGIN_INFO = {
    C.PI_NAME: "Blokcing Commands",
    C.PI_IMPORT_NAME: "XEP-0191",
    C.PI_TYPE: C.PLUG_TYPE_XEP,
    C.PI_MODES: C.PLUG_MODE_BOTH,
    C.PI_PROTOCOLS: ["XEP-0191"],
    C.PI_DEPENDENCIES: ["XEP-0060", "XEP-0376"],
    C.PI_MAIN: "XEP_0191",
    C.PI_HANDLER: "yes",
    C.PI_DESCRIPTION: _("""Implement the protocol to block users or whole domains"""),
}

NS_BLOCKING = "urn:xmpp:blocking"
IQ_BLOCK_PUSH = f'{C.IQ_SET}/block[@xmlns="{NS_BLOCKING}"]'
IQ_UNBLOCK_PUSH = f'{C.IQ_SET}/unblock[@xmlns="{NS_BLOCKING}"]'


class XEP_0191:

    def __init__(self, host):
        log.info(_("Blocking Command initialization"))
        host.register_namespace("blocking", NS_BLOCKING)
        self.host = host
        host.bridge.add_method(
            "blocking_list",
            ".plugin",
            in_sign="s",
            out_sign="as",
            method=self._block_list,
            async_=True,
        )
        host.bridge.add_method(
            "blocking_block",
            ".plugin",
            in_sign="ass",
            out_sign="",
            method=self._block,
            async_=True,
        )
        host.bridge.add_method(
            "blocking_unblock",
            ".plugin",
            in_sign="ass",
            out_sign="",
            method=self._unblock,
            async_=True,
        )

    def get_handler(self, client):
        return XEP_0191_Handler(self)

    @ensure_deferred
    async def _block_list(self, profile_key=C.PROF_KEY_NONE) -> List[str]:
        client = self.host.get_client(profile_key)
        blocked_jids = await self.block_list(client)
        return [j.full() for j in blocked_jids]

    async def block_list(self, client: SatXMPPEntity) -> Set[jid.JID]:
        await self.host.check_feature(client, NS_BLOCKING)
        iq_elt = client.IQ("get")
        iq_elt.addElement((NS_BLOCKING, "blocklist"))
        iq_result_elt = await iq_elt.send()
        try:
            blocklist_elt = next(iq_result_elt.elements(NS_BLOCKING, "blocklist"))
        except StopIteration:
            log.warning(f"missing <blocklist> element: {iq_result_elt.toXml()}")
            return []
        blocked_jids = set()
        for item_elt in blocklist_elt.elements(NS_BLOCKING, "item"):
            try:
                blocked_jid = jid.JID(item_elt["jid"])
            except (RuntimeError, AttributeError):
                log.warning(f"Invalid <item> element in block list: {item_elt.toXml()}")
            else:
                blocked_jids.add(blocked_jid)

        return blocked_jids

    def _block(self, entities: List[str], profile_key: str = C.PROF_KEY_NONE) -> str:
        client = self.host.get_client(profile_key)
        return defer.ensureDeferred(
            self.block(client, [jid.JID(entity) for entity in entities])
        )

    async def block(self, client: SatXMPPEntity, entities: List[jid.JID]) -> None:
        await self.host.check_feature(client, NS_BLOCKING)
        iq_elt = client.IQ("set")
        block_elt = iq_elt.addElement((NS_BLOCKING, "block"))
        for entity in entities:
            item_elt = block_elt.addElement("item")
            item_elt["jid"] = entity.full()
        await iq_elt.send()

    def _unblock(self, entities: List[str], profile_key: str = C.PROF_KEY_NONE) -> None:
        client = self.host.get_client(profile_key)
        return defer.ensureDeferred(self.unblock(client, [jid.JID(e) for e in entities]))

    async def unblock(self, client: SatXMPPEntity, entities: List[jid.JID]) -> None:
        await self.host.check_feature(client, NS_BLOCKING)
        iq_elt = client.IQ("set")
        unblock_elt = iq_elt.addElement((NS_BLOCKING, "unblock"))
        for entity in entities:
            item_elt = unblock_elt.addElement("item")
            item_elt["jid"] = entity.full()
        await iq_elt.send()

    def on_block_push(self, iq_elt: domish.Element, client: SatXMPPEntity) -> None:
        # TODO: send notification to user
        iq_elt.handled = True
        for item_elt in iq_elt.block.elements(NS_BLOCKING, "item"):
            try:
                entity = jid.JID(item_elt["jid"])
            except (KeyError, RuntimeError):
                log.warning(f"invalid item received in block push: {item_elt.toXml()}")
            else:
                log.info(f"{entity.full()} has been blocked for {client.profile}")
        iq_result_elt = xmlstream.toResponse(iq_elt, "result")
        client.send(iq_result_elt)

    def on_unblock_push(self, iq_elt: domish.Element, client: SatXMPPEntity) -> None:
        # TODO: send notification to user
        iq_elt.handled = True
        items = list(iq_elt.unblock.elements(NS_BLOCKING, "item"))
        if not items:
            log.info(f"All entities have been unblocked for {client.profile}")
        else:
            for item_elt in items:
                try:
                    entity = jid.JID(item_elt["jid"])
                except (KeyError, RuntimeError):
                    log.warning(
                        f"invalid item received in unblock push: {item_elt.toXml()}"
                    )
                else:
                    log.info(f"{entity.full()} has been unblocked for {client.profile}")
        iq_result_elt = xmlstream.toResponse(iq_elt, "result")
        client.send(iq_result_elt)


@implementer(iwokkel.IDisco)
class XEP_0191_Handler(xmlstream.XMPPHandler):

    def __init__(self, plugin_parent: XEP_0191):
        self.plugin_parent = plugin_parent

    def connectionInitialized(self):
        self.xmlstream.addObserver(
            IQ_BLOCK_PUSH, self.plugin_parent.on_block_push, client=self.parent
        )
        self.xmlstream.addObserver(
            IQ_UNBLOCK_PUSH, self.plugin_parent.on_unblock_push, client=self.parent
        )

    def getDiscoInfo(self, requestor, service, nodeIdentifier=""):
        return [disco.DiscoFeature(NS_BLOCKING)]

    def getDiscoItems(self, requestor, service, nodeIdentifier=""):
        return []
