#!/usr/bin/env python3

# Copyright (C) 2009-2022 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import Optional

from twisted.words.protocols.jabber import xmlstream
from twisted.words.xish import domish
from wokkel import disco
from zope.interface import implementer

from libervia.backend.core.constants import Const as C
from libervia.backend.core.i18n import _
from libervia.backend.core.log import getLogger

log = getLogger(__name__)


PLUGIN_INFO = {
    C.PI_NAME: "Fallback Indication",
    C.PI_IMPORT_NAME: "XEP-0428",
    C.PI_TYPE: "XEP",
    C.PI_MODES: C.PLUG_MODE_BOTH,
    C.PI_PROTOCOLS: ["XEP-0428"],
    C.PI_MAIN: "XEP_0428",
    C.PI_HANDLER: "yes",
    C.PI_DESCRIPTION: _("""Implementation of XEP-0428 (Fallback Indication)"""),
}

NS_FALLBACK = "urn:xmpp:fallback:0"


class XEP_0428(object):

    def __init__(self, host):
        log.info(_("XEP-0428 (Fallback Indication) plugin initialization"))
        host.register_namespace("fallback", NS_FALLBACK)

    def add_fallback_elt(
        self, message_elt: domish.Element, msg: Optional[str] = None
    ) -> None:
        """Add the fallback indication element

        @param message_elt: <message> element where the indication must be
            set
        @param msg: message to show as fallback
            Will be added as <body>
        """
        message_elt.addElement((NS_FALLBACK, "fallback"))
        if msg is not None:
            message_elt.addElement("body", content=msg)

    def has_fallback(self, message_elt: domish.Element) -> bool:
        """Tell if a message has a fallback indication"""
        try:
            next(message_elt.elements(NS_FALLBACK, "fallback"))
        except StopIteration:
            return False
        else:
            return True

    def get_handler(self, __):
        return XEP_0428_handler()


@implementer(disco.IDisco)
class XEP_0428_handler(xmlstream.XMPPHandler):

    def getDiscoInfo(self, __, target, nodeIdentifier=""):
        return [disco.DiscoFeature(NS_FALLBACK)]

    def getDiscoItems(self, requestor, target, nodeIdentifier=""):
        return []
