from typing import Tuple, overload


User = str
Host = str
Resource = str


# Note: these typings are incomplete and evolve as needed.
class JID:
    user: User
    host: Host
    resource: Resource

    @overload
    def __init__(self, str: str, tuple: None = None) -> None:
        ...

    @overload
    def __init__(self, str: None, tuple: Tuple[User, Host, Resource]) -> None:
        ...

    def userhost(self) -> str:
        ...

    def userhostJID(self) -> "JID":
        ...

    def full(self) -> str:
        ...
