from typing import Optional

from twisted.words.xish import domish


class BaseError(Exception):
    namespace: str
    condition: str
    text: Optional[str]
    textLang: Optional[str]
    appCondition: Optional[domish.Element]

class StanzaError(BaseError):
    ...
